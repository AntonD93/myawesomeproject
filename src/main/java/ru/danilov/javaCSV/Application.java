package ru.danilov.javaCSV;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import ru.danilov.javaCSV.view.console.Console;

@SpringBootApplication
public class Application implements CommandLineRunner {
    @Autowired
    Console console;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        console.print();
    }
}
