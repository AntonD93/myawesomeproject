package ru.danilov.javaCSV.db.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.danilov.javaCSV.db.model.DataImport;

import java.util.List;

public interface DataImportRepository extends JpaRepository<DataImport, Integer> {
    @Query("select ssoid, formid from data_import")//todo bad request
    List findAllInHour();
}
