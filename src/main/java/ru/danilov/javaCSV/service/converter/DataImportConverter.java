package ru.danilov.javaCSV.service.converter;

import org.springframework.stereotype.Service;
import ru.danilov.javaCSV.db.model.DataImport;
import ru.danilov.javaCSV.service.dto.DataImportDTO;

@Service
public class DataImportConverter {

    DataImport convertToDAO(DataImportDTO dataImportDTO){
        return new DataImport()
                .setSsoid(dataImportDTO.getSsoid())
                .setTs(dataImportDTO.getTs())
                .setGrp(dataImportDTO.getGrp())
                .setType(dataImportDTO.getType())
                .setSubtype(dataImportDTO.getSubtype())
                .setUrl(dataImportDTO.getUrl())
                .setOrgid(dataImportDTO.getOrgid())
                .setFormid(dataImportDTO.getFormid())
                .setCode(dataImportDTO.getCode())
                .setLtpa(dataImportDTO.getLtpa())
                .setSudirresponse(dataImportDTO.getSudirresponse())
                .setYmdh(dataImportDTO.getYmdh());
    }

    DataImportDTO convertToDTO(DataImport dataImportDAO){
        return new DataImportDTO()
                .setSsoid(dataImportDAO.getSsoid())
                .setTs(dataImportDAO.getTs())
                .setGrp(dataImportDAO.getGrp())
                .setType(dataImportDAO.getType())
                .setSubtype(dataImportDAO.getSubtype())
                .setUrl(dataImportDAO.getUrl())
                .setOrgid(dataImportDAO.getOrgid())
                .setFormid(dataImportDAO.getFormid())
                .setCode(dataImportDAO.getCode())
                .setLtpa(dataImportDAO.getLtpa())
                .setSudirresponse(dataImportDAO.getSudirresponse())
                .setYmdh(dataImportDAO.getYmdh());
    }
}
