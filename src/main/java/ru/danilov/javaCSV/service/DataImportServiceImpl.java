package ru.danilov.javaCSV.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.danilov.javaCSV.db.model.DataImport;
import ru.danilov.javaCSV.db.repository.DataImportRepository;
import ru.danilov.javaCSV.service.converter.DataImportConverter;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Service
public class DataImportServiceImpl implements DataImportService {

    @Autowired
    DataImportRepository dataImportRepository;
    @Autowired
    DataImportConverter dataImportConverter;

    @Override
    public void saveDataToDB(String str) {
        final int NUMBER_OF_TYPES_IN_ONE_LINE = 12;
        File file = new File(str);

        try (FileInputStream fis = new FileInputStream(file);
             BufferedReader br = new BufferedReader(new InputStreamReader(fis))){
            ArrayList list = new ArrayList();
            String strLine;

            while ((strLine = br.readLine()) != null){

                String[] strFromLine = strLine.split(";");

                for (String element : strFromLine) {
                    list.add(element);

                    if (list.size() == NUMBER_OF_TYPES_IN_ONE_LINE) {
                        writeDataToDB(list);
                        list.clear();
                    }
                }
            }

        }catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void findAll() {

        dataImportRepository.findAllInHour();
        //dataImportRepository.deleteAll();  //todo
        //System.out.println(dataImportRepository.findAll());
    }


    private void writeDataToDB(ArrayList list){
        DataImport dataImport = new DataImport();
        dataImport.setSsoid((String)list.get(0))
                .setTs((String)list.get(1))
                .setGrp((String)list.get(2))
                .setType((String)list.get(3))
                .setSubtype((String)list.get(4))
                .setUrl((String)list.get(5))
                .setOrgid((String)list.get(6))
                .setFormid((String)list.get(7))
                .setCode((String)list.get(8))
                .setLtpa((String)list.get(9))
                .setSudirresponse((String)list.get(10))
                .setYmdh((String)list.get(11));

        dataImportRepository.save(dataImport);
    }

    @Override
    public List findUsersAndFormsForLastHour() {
        return null;
    }

    @Override
    public List WTF() {
        return null;
    }

    @Override
    public List findTopFiveForms() {
        return null;
    }


}
